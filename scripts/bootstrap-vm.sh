#!/usr/bin/env bash
# Create a VirtualBox VM and install Arch on it.

set -Eeuo pipefail
# shellcheck disable=2154
trap 's=$?; echo "$0: Error on line $LINENO: $BASH_COMMAND"; exit $s' ERR
die() { echo ERROR: "$@" >&2; exit 1; }

ISO_FILE="/tmp/archlinux.iso"

get_iso() {
    local ISO_DATE
    ISO_DATE="$(date '+%Y.%m').01"
    local ISO_URL="https://archmirror1.octyl.net/iso/$ISO_DATE/archlinux-$ISO_DATE-x86_64.iso"

    if [[ ! -f "$ISO_FILE" ]]; then
        echo "Downloading ISO to $ISO_FILE"
        curl --fail "$ISO_URL" -o "$ISO_FILE"
    else
        echo "ISO already present at $ISO_FILE, skipping download"
    fi
}

create_vm() {
    local DIR RAM V_RAM HDD
    read -re -i "$HOME/VirtualBox VMs/$NAME" -p "VM directory: " DIR
    read -re -i 2048 -p "RAM: " RAM
    read -re -i 128 -p "Video RAM: " V_RAM
    read -re -i 32000 -p "HDD (in megabytes): " HDD
    local VDI="$DIR/$NAME.vdi"

    if [[ -d "$DIR" ]]; then
        local DO_DELETE
        read -re -p "VM already exists, delete and recreate it? [Y/n] " DO_DELETE
        if [[ "$DO_DELETE" == "n" || "$DO_DELETE" == "N" ]]; then
            die "Cancelled"
        fi
        VBoxManage unregistervm --delete "$NAME"
    fi

    VBoxManage createvm --name "$NAME" --ostype "ArchLinux_64" --register --basefolder "$DIR"
    VBoxManage modifyvm "$NAME" --memory "$RAM" --vram "$V_RAM"
    VBoxManage modifyvm "$NAME" --graphicscontroller vmsvga
    VBoxManage modifyvm "$NAME" --firmware efi64
    VBoxManage modifyvm "$NAME" --cpus 2 --nested-hw-virt on

    # Hard drive
    VBoxManage createhd --filename "$VDI" --size "$HDD" --format VDI
    VBoxManage storagectl "$NAME" --name "SATA" --add sata --controller IntelAHCI
    VBoxManage storageattach "$NAME" --storagectl "SATA" --port 0 --device 0 --type hdd --medium "$VDI"

    # ISO
    VBoxManage storagectl "$NAME" --name "IDE" --add ide --controller PIIX4
    VBoxManage storageattach "$NAME" --storagectl "IDE" --port 1 --device 0 --type dvddrive --medium "$ISO_FILE"
    VBoxManage modifyvm "$NAME" --boot1 dvd --boot2 disk
}


get_iso
echo ""
echo "CONFIG:"
read -re -i arch-test -p "VM name: " NAME
create_vm

VBoxManage startvm "$NAME"

# Kill child processes (i.e. install script server) on exit
trap 'trap - SIGTERM && kill -- -$$' SIGINT SIGTERM EXIT

# Serve install script locally (so I can test script changes without pushing).
echo ""
echo "Serving install script"
python3 -m http.server 8080 --directory "$(dirname "${BASH_SOURCE[0]}")" &
# Also serve local repo for testing/development if present - see scripts/docker-local.sh
LOCAL_REPO=/tmp/arch-as-code-mount/repo
if [[ -d "$LOCAL_REPO" ]]; then
    echo "Found local repo to serve: $LOCAL_REPO"
    python3 -m http.server 8081 --directory "$LOCAL_REPO" &
fi

read -r -d '' -p "Press ctrl+c to quit"
