#!/usr/bin/env bash
# Scripted Arch Linux installation.
# WARNING: DO NOT RUN THIS WITHOUT UNDERSTANDING IT OR YOU WILL LOSE DATA!
#
# Usage: bash -c "$(curl -sL bit.ly/2XAsztj)"
#
# Inspired by https://disconnected.systems/blog/archlinux-installer

set -Eeuo pipefail
# shellcheck disable=2154
trap 's=$?; echo "$0: Error on line $LINENO: $BASH_COMMAND"; exit $s' ERR
die() { echo ERROR: "$@" >&2; exit 1; }

LOG=/tmp/arch-as-code-install.log

init() {
    exec > >(tee "$LOG") 2>&1
    [[ -d /sys/firmware/efi/efivars ]] || die "System is not booted in EFI mode"
    timedatectl set-ntp true

    # Speed up package installs by picking fastest mirrors.
    reflector --verbose \
              --country='United States' --protocol=https --score=10 --sort=rate \
              --save=/etc/pacman.d/mirrorlist
    pacman -Sy --noconfirm dialog
}

# Get user input for password with confirmation.
# Response will be stored in $REPLY.
# $1 = prompt string (trailing colon/space will be added).
read_pw() {
    local FIRST SECOND
    while true; do
        read -rs -p "$1: " FIRST
        echo
        read -rs -p "$1 (again): " SECOND
        echo

        if [[ "$FIRST" == "$SECOND" ]]; then
            break
        else
            echo "Inputs did not match, try again"
        fi
    done
    REPLY="$FIRST"
}

# User input is done early on so that rest of install can be done unattended.
user_input() {
    read -re -p "Testing repo (optional - leave empty for none): " LOCAL_REPO
    read_pw "Disk encryption password" && DISK_PW="$REPLY"
    read -re -p "Hostname: " HOST
    read -re -p "User: " USER
    read_pw "User password" && USER_PW="$REPLY"
    read_pw "Root password" && ROOT_PW="$REPLY"
}

confirm_or_die() {
    PROMPT=$1
    local CONFIRM
    read -re -n 1 -p "$PROMPT [y/N] " CONFIRM
    if [[ "$CONFIRM" != "y" && "$CONFIRM" != "Y" ]]; then
        die "Cancelled."
    fi
}

PARTITION_EFI=/dev/disk/by-partlabel/EFI
PARTITION_LUKS=/dev/disk/by-partlabel/cryptsystem
# noatime is particularly important for btrfs, since it's copy-on-write.
BTRFS_OPTS=defaults,noatime,compress=zstd
BTRFS_SWAP_OPTS=defaults,noatime

# Returns 1 for clean install, 0 to preserve existing /home.
detect_install() {
    local USE_EXISTING
    if [[ ! -e "$PARTITION_EFI" || ! -e "$PARTITION_LUKS" ]]; then
        echo "No existing install detected."
        USE_EXISTING="n"
    else
        read -re -n 1 -p "Detected existing install, keep /home? [y/n] " USE_EXISTING
    fi

    if [[ "$USE_EXISTING" == "n" || "$USE_EXISTING" == "N" ]]; then
        echo "WARNING: ABOUT TO DO A FRESH INSTALL. THIS IS DANGEROUS!"
        sleep 5
        confirm_or_die "Delete all data and continue with fresh install?"
        sleep 1
        confirm_or_die "Are you positive?"
        return 1
    fi
    echo "Preserving /home from existing install."
}

get_install_disk() {
    local DISK_LIST
    DISK_LIST=$(lsblk --nodeps --paths --list --noheadings --sort size --output name,size \
                    | grep -Ev "boot|rpmb|loop" \
                    | tac)
    # Intentionally expanding list so that dialog interprets it as menu items.
    # shellcheck disable=2086
    DISK=$(dialog --stdout --menu "Select install disk" 0 0 0 $DISK_LIST)
    clear

    echo "Existing partitions:"
    lsblk --noheadings --paths
    echo ""
    confirm_or_die "WARNING: You will lose all data on $DISK. Continue?"
}

partition_clean() {
    get_install_disk

    # Keep in mind that GPT partition labels (/dev/disk/by-partlabel) and
    # filesystem labels (/dev/disk/by-label) are two different things.
    # https://wiki.archlinux.org/index.php/Persistent_block_device_naming
    sgdisk --zap-all "$DISK"
    sgdisk --clear \
           --new=1:0:+550MiB --typecode=1:ef00 --change-name=1:EFI \
           --new=2:0:0 --typecode=2:8300 --change-name=2:cryptsystem \
           "$DISK"
    # /dev/disk/by-partlabel takes a sec to update
    sleep 1

    wipefs --all "$PARTITION_EFI" "$PARTITION_LUKS"
    # echo -n to prevent trailing newline from becoming part of password.
    echo -n "$DISK_PW" | cryptsetup luksFormat --type luks2 "$PARTITION_LUKS" -
    echo -n "$DISK_PW" | cryptsetup open "$PARTITION_LUKS" system

    # btrfs references:
    # https://btrfs.wiki.kernel.org/index.php/SysadminGuide
    # https://wiki.archlinux.org/index.php/Btrfs
    mkfs.btrfs --label system /dev/mapper/system
    # *Temporarily* mount the top-level subvolume to create subvolumes which
    # will then be mounted for normal use. The top-level subvolume should only
    # be mounted when needed, such as when creating subvolumes or snapshots, not
    # for day-to-day normal use.
    mount -o "$BTRFS_OPTS" LABEL=system /mnt
    btrfs subvolume create /mnt/home
    btrfs subvolume create /mnt/snapshots-home
}

partition_preserve_home() {
    echo ""
    echo "Decrypting existing install"
    until echo -n "$DISK_PW" | cryptsetup open "$PARTITION_LUKS" system; do
        echo "Disk encryption password did not match existing, try again."
        read -rs -p "Password: " DISK_PW
        echo ""
    done
    echo "Successfully decrypted"

    # id 5 = top-level subvolume
    mount -o "$BTRFS_OPTS,subvolid=5" /dev/mapper/system /mnt
    if [[ ! -d /mnt/root \
              || ! -d /mnt/snapshots-root \
              || ! -d /mnt/swap \
              || ! -d /mnt/home \
              || ! -d /mnt/snapshots-home ]]; then
        ls -la /mnt
        die "Unknown btrfs subvolume layout, refusing to continue." \
            "\nReboot and run this script again to do a clean install if desired."
    fi

    # Systemd creates some subvolumes automatically (e.g. /var/lib/machines and
    # /var/lib/portables), so those need to be deleted *before* the root
    # partition.
    # local DELETE_QUEUE
    # mapfile -t DELETE_QUEUE < <(btrfs subvolume list -o /mnt/root | awk '{print $NF}')
    local DELETE_QUEUE=()
    while IFS= read -r S; do
        DELETE_QUEUE+=("/mnt/$S")
    done < <(btrfs subvolume list -o /mnt/root | awk '{print $NF}')

    DELETE_QUEUE+=(/mnt/root /mnt/snapshots-root /mnt/swap)
    btrfs subvolume delete --verbose --commit-after "${DELETE_QUEUE[@]}"

    wipefs "$PARTITION_EFI"
}

# Final partionining steps, common to both clean and existing installs.
finish_partitioning() {
    btrfs subvolume create /mnt/root
    btrfs subvolume create /mnt/snapshots-root
    # Swap and snapshots don't play nicely (unsurprisingly), so make a
    # separate subvolume exclusively for swap.
    btrfs subvolume create /mnt/swap
    umount -R /mnt

    mkfs.fat -F32 -n EFI "$PARTITION_EFI"

    mount -o "$BTRFS_OPTS,subvol=root" LABEL=system /mnt
    mkdir /mnt/boot /mnt/swap /mnt/home
    mount -o "$BTRFS_SWAP_OPTS,subvol=swap" LABEL=system /mnt/swap
    mount -o "$BTRFS_OPTS,subvol=home" LABEL=system /mnt/home
    mount "$PARTITION_EFI" /mnt/boot

    create_swap
}

create_swap() {
    # Compute swap size based on RAM.
    local RAM_SIZE SWAP_SIZE
    RAM_SIZE=$(free --mega | awk '/Mem:/{print $2}')
    # Fedora recommends that swap should be 2GB bigger than RAM:
    # https://docs.fedoraproject.org/en-US/Fedora/14/html/Storage_Administration_Guide/ch-swapspace.html
    SWAP_SIZE=$((RAM_SIZE + 2048))

    local SWAPFILE="/mnt/swap/swapfile"
    # +C attribute disables copy-on-write. When set on a directory like this,
    # it applies to all new files created within it (existing files may or may
    # not be affected - "man chattr" say it's more or less undefined behavior).
    # This could also be set on the swap file directly, but setting it on the
    # directory will survive swap file deletion/re-creation (which shouldn't
    # happen, but still).
    chattr +C "$(dirname "$SWAPFILE")"
    btrfs property set "$(dirname $SWAPFILE)" compression none
    truncate -s 0 "$SWAPFILE"
    fallocate --length "$SWAP_SIZE"M "$SWAPFILE"
    chmod 600 "$SWAPFILE"
    mkswap "$SWAPFILE"
    swapon "$SWAPFILE"
}

get_repo_servers() {
    # Make life easier while testing changes to my packages.
    if [[ "$LOCAL_REPO" != "" ]]; then
        echo "Server = $LOCAL_REPO"
    fi
    echo "Server = https://chasecaleb-arch-pkgs.s3.amazonaws.com/x86_64/"
}

install_system() {
    cat <<EOF >> /etc/pacman.conf
[chasecaleb]
SigLevel = Optional TrustAll
$(get_repo_servers)
EOF
    # Need to fetch db file for my repo, or else future pacman commands will fail.
    pacman -Sy

    pacstrap /mnt chasecaleb-base
    genfstab -U /mnt >> /mnt/etc/fstab

    # NOTE: chasecaleb-base provides /etc/mkinitcpio.conf and /boot/loader/* files. In general,
    # packages should never contain anything under /boot... but my use-case is special because these
    # are personal packages that target a specific, known install/setup.
    arch-chroot /mnt bootctl install
}

install_boot_loader() {
    echo "default arch" > /mnt/boot/loader/loader.conf

    cat <<EOF > /mnt/boot/loader/entries/arch.conf
title    Arch Linux - Zen
linux    /vmlinuz-linux-zen
initrd   /intel-ucode.img
initrd   /initramfs-linux-zen.img
options  cryptdevice=PARTLABEL=cryptsystem:luks:allow-discards root=/dev/mapper/luks rootflags=subvol=root rw
EOF
    cat <<EOF > /mnt/boot/loader/entries/arch-fallback.conf
title    Arch Linux - Zen Fallback
linux    /vmlinuz-linux-zen
initrd   /intel-ucode.img
initrd   /initramfs-linux-zen-fallback.img
options  cryptdevice=PARTLABEL=cryptsystem:luks:allow-discards root=/dev/mapper/luks rootflags=subvol=root rw
EOF
}

configure_system() {
    echo "$HOST" > /mnt/etc/hostname

    # Ideally locale.conf should go in my base package, but that causes issues
    # because /etc/locale.conf may or may not already exist on the system -
    # in the case of a fresh install like this it does not exist, whereas when
    # building from a Docker image it already does. Fortunately I don't ever
    # change this file, so creating it during install is good enough.
    echo "LANG=en_US.UTF-8" > /mnt/etc/locale.conf

    arch-chroot /mnt ln -sf /usr/share/zoneinfo/US/Central /etc/localtime
    arch-chroot /mnt timedatectl set-ntp true
    arch-chroot /mnt timedatectl set-local-rtc 1
    arch-chroot /mnt hwclock --systohc

    # This logic isn't foolproof, but it's a quick-and-dirty safeguard to catch
    # if user mistyped the username at the start of this script.
    # Interesting factoid: $UID is a reserved read-only variable in Bash.
    local THE_UID=1000
    # Need to use different FD because of nested read. Yay Bash.
    while IFS= read -rd '' DIR <&3; do
        echo ""
        echo "WARNING: Found home directory with initial account's UID ($THE_UID): $DIR"
        echo "However, at the start of this script you entered user $USER"
        NEW_USER=$(basename "$DIR")
        local CONFIRM
        read -re -p "Create user $NEW_USER instead of $USER? [Y/n] " CONFIRM </dev/tty
        echo ""
        if [[ "$CONFIRM" != "n" && "$CONFIRM" != "N" ]]; then
            USER=$NEW_USER
        fi
    done 3< <(find /mnt/home -maxdepth 1 -type d -user "$THE_UID" -print0 | sort -z)
    echo "Creating user $USER"

    # Use explicit uid and gid to ensure that they are the same across installs.
    arch-chroot /mnt groupadd --gid 1000 "$USER"
    arch-chroot /mnt useradd --create-home --shell /usr/bin/zsh --groups wheel,uucp,rfkill,video --uid "$THE_UID" --gid 1000 "$USER"
    arch-chroot /mnt chsh -s /usr/bin/zsh root
    arch-chroot /mnt /bin/bash -c "echo \"$USER:$USER_PW\" | chpasswd"
    arch-chroot /mnt /bin/bash -c "echo \"root:$ROOT_PW\" | chpasswd"
}

configure_device() {
    # Not installed to chroot because this is only needed temporarily
    pacman -S --noconfirm dmidecode
    local SYSTEM
    SYSTEM=$(dmidecode -s system-manufacturer)

    local PACKAGE
    if [[ "$SYSTEM" == "LENOVO" ]]; then
        PACKAGE="chasecaleb-laptop"
    elif [[ "$SYSTEM" == "VMware"* ]]; then
        PACKAGE="chasecaleb-vmware"
    elif [[ "$SYSTEM" == "Parallels"* ]]; then
        PACKAGE="chasecaleb-work-mac"
    fi

    if [[ "${PACKAGE:-}" != "" ]]; then
        echo "Installing additional device-specific package: $PACKAGE"
        arch-chroot /mnt pacman -S --noconfirm "$PACKAGE"
    fi
}

init
user_input

if detect_install; then
    partition_preserve_home
else
    partition_clean
fi
finish_partitioning

install_system
configure_system
configure_device

LOG_DEST=/mnt/var/log/$(basename "$LOG")
cp "$LOG" "$LOG_DEST"
echo "Log copied to: $LOG_DEST"
echo "Install finished! Run 'reboot' to complete"
# Sleep prevents last line of output from potentially showing up after the
# prompt, since output is piped through tee for logging.
sleep 1
