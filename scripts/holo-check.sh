#!/usr/bin/env bash
# Helper while writing .holoscript files (holo-cm).
# Runs .holoscript against the original contents of a package file.

set -Eeuo pipefail
die() { echo -e "$0" ERROR: "$@" >&2; exit 1; }
# shellcheck disable=2154
trap 's=$?; die "line $LINENO - $BASH_COMMAND"; exit $s' ERR

CMD=$1
SCRIPT=$(realpath "$2")
FILE=$(sed -e 's@.*/holo-files@@' -e 's/\.holoscript$//' <<< "$SCRIPT")
PKG_NAME=$(pacman -Q --quiet --owns "$FILE")

sudo pacman -S --downloadonly --noconfirm "$PKG_NAME" >/dev/null
PKG_ARCHIVE=$(pacman -S --print "$PKG_NAME" | sed 's@file://@@')
# Strip leading slash from filename because package archives are not absolute.
ORIGINAL=$(tar -I zstd -xOf "$PKG_ARCHIVE" "${FILE/#\//}")

if [[ "$CMD" == "original" ]]; then
    echo "$ORIGINAL"
elif [[ "$CMD" == "diff" ]]; then
    RESULT=$("$SCRIPT" <<< "$ORIGINAL")
    diff --color <(echo "$ORIGINAL") <(echo "$RESULT") || true
else
    die "Unknown command: $CMD"
fi
