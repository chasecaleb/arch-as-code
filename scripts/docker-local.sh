#!/usr/bin/env bash
# Docker build-and-run wrapper for my own usage during development.
# This is quick-and-dirty, so you'll need to modify if to work for you.

set -Eeuo pipefail
die() { echo -e "$0" ERROR: "$@" >&2; exit 1; }
# shellcheck disable=2154
trap 's=$?; die "line $LINENO - $BASH_COMMAND"; exit $s' ERR

HOST_PROJECT_DIR=$(realpath "${BASH_SOURCE%/*}/../")

COMMAND=$1
shift
OPT_CACHE=1 OPT_DEPS=1 OPT_TARGET="packages/*"
while true; do
    case ${1:-} in
        --no-cache)
            OPT_CACHE=0
            ;;
        --no-deps)
            OPT_DEPS=0
            ;;
        --target)
            OPT_TARGET=$2
            shift
            ;;
        --aws-config)
            OPT_AWS_CONFIG=$2
            shift
            ;;
        --)
            # Preserve remaining args without parsing
            shift
            break
            ;;
        -?*)
            die "Unknown option: $1"
            ;;
        *)
            # No more options, done parsing
            break
            ;;
    esac
    shift
done

NAME="local-builder"

do_run() {
    # Always build image (with cache) to make sure that this runs with up to date build scripts.
    # I've forgotten this one too many times. Fortunately if the image is up to date this will
    # complete almost instantly.
    "${BASH_SOURCE[0]}" image

    # Don't want to modify the project source, so make a clean copy of it.
    HOST_PROJECT_COPY=/tmp/arch-as-code-mount
    if [[ -e "$HOST_PROJECT_COPY" ]]; then
        rm -rf "$HOST_PROJECT_COPY"
    fi
    cp -r "$HOST_PROJECT_DIR" "$HOST_PROJECT_COPY"
    # Don't use stale package files (i.e. ones built on host while testing/experimenting)
    find "$HOST_PROJECT_COPY" -type f -name '*.pkg.*' -delete

    DOCKER_ENV_ARGS=(
        --env "PROJECT_DIR=/project"
        --env "REPO_BUCKET_NAME=chasecaleb-arch-pkgs"
        --env "REPO_NAME=chasecaleb"
    )
    # This will probably break if you aren't me, adjust this if so.
    # Note: There may or may not be whitespace around the "=".
    if [[ "${OPT_AWS_CONFIG:-}" != "" ]]; then
        AWS_CREDENTIALS=$(sed '0,/arch-pkg-builder/d' "$OPT_AWS_CONFIG" | \
                              grep -i -e "access_key" | \
                              awk -F'=' '{print toupper($1) "=" $2}' | \
                              sed 's/\s*=\s*/=/') || \
            echo "No AWS credentials detected, will not be able to push to repo." >&2
        DOCKER_ENV_ARGS+=(
            --env "$(grep ACCESS_KEY_ID <<< "$AWS_CREDENTIALS")"
            --env "$(grep SECRET_ACCESS_KEY <<< "$AWS_CREDENTIALS")"
        )
    fi

    docker run --rm -it "${DOCKER_ENV_ARGS[@]}" \
           --mount type=bind,source="$HOST_PROJECT_COPY",target=/project \
           pkgbuilder:latest \
           bash -c "$@"
}

if [[ "$COMMAND" == "image" ]]; then
    if  [[ "$OPT_CACHE" == 0 ]]; then
        docker build "$HOST_PROJECT_DIR/builder" --tag pkgbuilder:latest --no-cache --pull
    else
        docker build "$HOST_PROJECT_DIR/builder" --tag pkgbuilder:latest --pull
    fi
elif [[ "$COMMAND" == "rm" ]]; then
    docker rm --force "$NAME"
elif [[ "$COMMAND" == "packages" ]]; then
    if (( OPT_DEPS )); then
        DEPS_ARG="--syncdeps"
    else
        DEPS_ARG="--nodeps"
    fi
    # shellcheck disable=2016
    do_run 'build.sh --init && for P in "$PROJECT_DIR"/$1/PKGBUILD; do cd "$(dirname $P)" && aur build --force -- --clean $2; done' bash "$OPT_TARGET" "$DEPS_ARG"
elif [[ "$COMMAND" == "add-aur-pkg" ]]; then
    # shellcheck disable=2016
    do_run 'add-aur-pkg.sh "$PROJECT_DIR" "$1"' bash "$1"
    cp -r "$HOST_PROJECT_COPY/aur" "$HOST_PROJECT_DIR"
    if [[ "${SUDO_UID:-}" != "" && "${SUDO_GID:-}" != "" ]]; then
        chown -R "$SUDO_UID:$SUDO_GID" "$HOST_PROJECT_DIR/aur/$1"
    fi
elif [[ "$COMMAND" == "bash" ]]; then
    do_run "bash"
else
    die "Unknown command"
fi
