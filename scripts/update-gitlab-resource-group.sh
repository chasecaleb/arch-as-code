#!/usr/bin/env bash
# Because GitLab doesn't provide a way to control resource_group process mode (i.e. execution order)
# from .gitlab-ci.yml directly, and the default of "unordered" is potentially problematic in this
# project.
#
# See https://docs.gitlab.com/ee/api/resource_groups.html#edit-an-existing-resource-group

set -Eeuo pipefail
die() { echo -e "$0" ERROR: "$@" >&2; exit 1; }
# shellcheck disable=2154
trap 's=$?; die "line $LINENO - $BASH_COMMAND"; exit $s' ERR

resource_group=$1
mode=$2

echo "Updating resource group $resource_group to process mode $mode"
curl -fsS --request PUT --header "PRIVATE-TOKEN: $API_TOKEN" \
    --data "process_mode=$mode" \
    "$CI_API_V4_URL/projects/$CI_PROJECT_ID/resource_groups/$resource_group"
# API response doesn't have a trailing newline.
echo ""
