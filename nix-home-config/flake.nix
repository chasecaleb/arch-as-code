{
  description = "Home Manager configuration for chasecaleb";

  inputs = {
    home-manager.url = "github:nix-community/home-manager";
    nixpkgs.url = "github:nixos/nixpkgs/nixos-unstable";
    home-manager.inputs.nixpkgs.follows = "nixpkgs";
  };

  outputs = { nixpkgs, home-manager, ... }:
    let
      system = "x86_64-linux";
      username = "caleb";
    in
    {
      homeConfigurations.${username} = home-manager.lib.homeManagerConfiguration {
        configuration = { config, pkgs, ... }: {
          home.username = "caleb";
          home.homeDirectory = "/home/caleb";
          programs.home-manager.enable = true;

          home.packages = [
            pkgs.rnix-lsp
          ];
        };

        inherit system username;
        homeDirectory = "/home/${username}";
        # Check release notes for stateVersion info here:
        # https://nix-community.github.io/home-manager/release-notes.html
        stateVersion = "22.05";
      };
    };
}
