# Arch Linux, Infrastructure-As-Code Style
[![pipeline status](https://gitlab.com/chasecaleb/arch-as-code/badges/master/pipeline.svg)](https://gitlab.com/chasecaleb/arch-as-code/-/commits/master)

This is my personal Arch Linux setup. **Attempting to use it as-is will likely cause you pain and data loss**.

# Try it out in a VM
1. Create a VM with the Arch Linux install ISO attached to it
   * You can use `scripts/bootstrap-vm.sh` to automate this if you'd like
1. Boot the VM
1. **In the VM**, run `bash -c "$(curl -sL bit.ly/2XAsztj)"` (which is the master branch version of `scripts/install.sh`)
1. Done! (Yes, that's really all it takes)

# Manual Steps (after install)
> Caveat: These steps are my own notes and likely don't apply to anyone other than me

1. Wireless networks (if relevant): run `sudo chasecaleb_wireless_setup YOUR_SSID YOUR_PASSWORD`
1. Sign in to Firefox
1. Create ssh keys (via `ssh-keygen -t ed25519`) and `~/.ssh/config` and upload to Gitlab, etc
1. Run `chasecaleb-git-repos`
1. Run `~/code/gpg-keys/import.sh`
1. Connect Signal app to phone

# Packaging Notes
These packages are for my own use and not intended for distribution or consumption by anyone else, so I violate some packaging conventions and general "best practices" when pragmatic.

* Version numbers: For my own packages, I derive version numbers from the number of commits and the commit hash. This is also how git-based packages in Arch User Repository typically work. *Caveat*: anything that causes the commit count to decrease (e.g. squashing) on master will cause issues, so don't do that.

## Related Reading
* https://disconnected.systems/blog/archlinux-installer (3 parts + Reddit link)
* http://holocm.org
* https://wiki.archlinux.org/index.php/Installation_guide
* https://wiki.archlinux.org/index.php/General_recommendations
* Similar repos
    * https://github.com/Earnestly/pkgbuilds/tree/master/system-config
    * https://github.com/Foxboron/PKGBUILDS/tree/master/foxboron-system
