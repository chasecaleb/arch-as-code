#!/usr/bin/env bash
# Create merge requests for any packages that need to be updated.
# This is run via Gitlab CI, so some env variables are assumed.

set -Eeuo pipefail
die() { echo -e "$0" ERROR: "$@" >&2; exit 1; }
# shellcheck disable=2154
trap 's=$?; die "line $LINENO - $BASH_COMMAND"; exit $s' ERR

# Make Gitlab api call, relative to project.
api_call() {
    local RELATIVE_URL=$1
    shift
    local API=$CI_API_V4_URL/projects/$CI_PROJECT_ID
    curl -fsSL --header "Private-Token: $API_TOKEN" "$@" "$API/$RELATIVE_URL"
}

create_mr() {
    BODY=$(mktemp)
    cat << EOF > "$BODY"
{
  "id": $CI_PROJECT_ID,
  "source_branch": "$1",
  "target_branch": "master",
  "title": "$2",
  "description": "Created by: $CI_JOB_URL",
  "assignee_id": $GITLAB_USER_ID,
  "remove_source_branch": true,
  "squash": true
}
EOF
    api_call "merge_requests" --request POST --header "Content-Type: application/json" --data-binary "@$BODY"
}

remove_package() {
    local PKG_NAME=$1
    local PKG_DIR=$PROJECT_DIR/aur/$PKG_NAME
    if [[ -d "$PKG_DIR" ]]; then
        echo "Removed package: $PKG_NAME"
        rm -r "$PKG_DIR"
    else
        echo "Package was already removed: $PKG_NAME ($PKG_DIR)"
    fi
}

update_package() {
    PKG_NAME=$1
    echo "Checking package for updates: $PKG_NAME"

    BRANCH_NAME="update-$PKG_NAME"
    # Branch might already exist (e.g. if there is an outstanding/unmerged update)
    git checkout "$BRANCH_NAME" 2>/dev/null || {
        git checkout master
        git checkout -b "$BRANCH_NAME"
    }

    PKG_INFO=$(curl -fsSL --get "https://aur.archlinux.org/rpc.php?v=5&type=info" --data-urlencode "arg=$1")
    if [[ "$(jq -e -r '.results | length' <<< "$PKG_INFO")" == 0 ]]; then
        remove_package "$PKG_NAME"
        MESSAGE="[Automated] Remove deleted package: $PKG_NAME"
    elif [[ "$(jq -e -r '.results[0].Maintainer' <<< "$PKG_INFO" )" == "null" ]]; then
        remove_package "$PKG_NAME"
        MESSAGE="[Automated] Remove orphaned package: $PKG_NAME"
    else
        add-aur-pkg.sh "$PROJECT_DIR" "$PKG_NAME"
        MESSAGE="[Automated] Update package: $PKG_NAME"
    fi


    if [[ $(git status --porcelain .) ]]; then
        git add .
        git commit --message "$MESSAGE"
        git push origin "$BRANCH_NAME"
    fi

    # Check for existing MRs to avoid creating duplicates.
    EXISTING_MRS=$(api_call "merge_requests?state=opened" --get --data-urlencode "source_branch=$BRANCH_NAME")
    MR_COUNT=$(jq -e -r '. | length' <<< "$EXISTING_MRS")

    # Need to actually compare against master, not rely on git status from
    # earlier, because the branch might have been updated without an MR
    # getting created (e.g. if this script fails partway through).
    if ! git diff --quiet origin/master &&  [[ "$MR_COUNT" == "0" ]]; then
        MR_URL=$(create_mr "$BRANCH_NAME" "$MESSAGE" | jq -e -r '.web_url')
        echo "Created merge request for $PKG_NAME: $MR_URL"
    elif [[ "$MR_COUNT" != "0" ]]; then
        MR_URL=$(jq -e -r '.[0] | .web_url' <<< "$EXISTING_MRS")
        echo "Existing merge request for $PKG_NAME: $MR_URL"
    else
        echo "Package is already up to date: $PKG_NAME"
    fi
}

git fetch
while IFS= read -rd '' PKG_DIR; do
    (update_package "$(basename "$PKG_DIR")")
    echo ""
done < <(find "$PROJECT_DIR/aur" -maxdepth 1 -mindepth 1 -print0 | sort -z)
