#!/usr/bin/env bash
# Does runtime setup, then switches to non-privilege user and execs provided args.
# This must be run as root.

set -Eeuo pipefail
die() { echo -e "$0" ERROR: "$@" >&2; exit 1; }
# shellcheck disable=2154
trap 's=$?; die "line $LINENO - $BASH_COMMAND"; exit $s' ERR

if [[ -z "$REPO_BUCKET_NAME" || -z "$REPO_NAME" ]]; then
    die "Missing env variables: REPO_BUCKET_NAME and/or REPO_NAME"
fi
export REPO_BUCKET_DIR=x86_64
export REPO_BUCKET=s3://$REPO_BUCKET_NAME/$REPO_BUCKET_DIR

# Configure pacman at runtime instead of build due to env variables. The [chasecaleb] server was
# already added to pacman.conf as part of the Docker buildfile, so now we have to add the local
# server *before* the remote server.
#
# Order of the servers is important:
# - Local must be first to take precedence for e.g. updated packages.
# - Remote s3 comes after as a fallback for existing packages that haven't been built locally.
#
# This makes it so the build script can pull just the repo db files (not all packages) from s3
# to local, which saves a lot of time and money.
sed -i "/chasecaleb-arch-pkgs\.s3/i Server = file://$PROJECT_DIR/repo" /etc/pacman.conf
chown -R pkgbuilder "$PROJECT_DIR"

exec gosu pkgbuilder "$@"
