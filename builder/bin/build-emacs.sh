#!/usr/bin/env bash
# Build Emacs PKGBUILDs.
#
# These are handled specially because they live in my emacs.d repo and are a bit... unique.

set -Eeuo pipefail
die() { echo -e "$0" ERROR: "$@" >&2; exit 1; }
# shellcheck disable=2154
trap 's=$?; die "line $LINENO - $BASH_COMMAND"; exit $s' ERR

source "build-utils.sh"
emacs_project_dir=/tmp/emacs-repo
pkgbuild_dir=$emacs_project_dir/scripts/

checkout_emacs_repo() {
    git clone https://gitlab.com/chasecaleb/emacs.d.git "$emacs_project_dir"
    cd "$emacs_project_dir"
    git reset --hard "$EMACS_REPO_REVISION"
    cd - >/dev/null
}

init() {
    update_pacman_and_repo_dbs
    checkout_emacs_repo
    update_all_pkgvers "$pkgbuild_dir"
}

opt_init=0 opt_build=0 opt_upload=0
while true; do
    if [[ $# == 0 ]]; then
        # No more options, done parsing
        break
    fi
    case ${1:-} in
        --init)
            opt_init=1
            ;;
        --build)
            opt_build=1
            ;;
        --upload)
            opt_upload=1
            ;;
        *)
            die "Unknown argument: $1"
            ;;
    esac
    shift
done

# Invoking in subshells to isolate state (global vars, cd, etc)
if ((opt_init)); then
    (init)
fi
if ((opt_build)); then
    (build_updated "$pkgbuild_dir" --nodeps)
fi
if ((opt_upload)); then
    (upload)
fi
