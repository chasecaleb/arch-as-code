#!/usr/bin/env bash

set -Eeuo pipefail
# shellcheck disable=2154
trap 's=$?; echo "$0: Error on line $LINENO: $BASH_COMMAND"; exit $s' ERR
die() { echo -e ERROR: "$@" >&2; exit 1; }

source "build-utils.sh"

init() {
    update_pacman_and_repo_dbs

    # Update pkgver in PKGBUILD's and generate .SRCINFO. I do this only for my
    # custom packages (not AUR) for two reasons:
    # 1) I generate merge requests to update AUR packages (blindly updating is
    #    a security concern), so I already take care of it there. I don't do
    #    that for my own packages, so it's either do it here or remember to do
    #    it by hand when making PKGBUILD changes.
    # 2) Efficiency: makepkg has to download sources before calling pkgver(),
    #    which can take a loooong time for some AUR packages.
    #
    # NOTE: this needs to happen in init because there are several parts of this
    # script that depend on package versions being correct (e.g. prune).
    update_all_pkgvers "$PROJECT_DIR/packages"
}

build_aur() {
    echo "Building AUR packages"
    build_updated "$PROJECT_DIR/aur" --syncdeps
}

build_pkgs() {
    echo "Building packages"
    build_updated "$PROJECT_DIR/packages" --nodeps
}

# Turns out Amazon doesn't particularly care about implementing HTTP spec properly in S3.
# https://forums.aws.amazon.com/thread.jspa?threadID=55746
# https://git.uplinklabs.net/steven/projects/archlinux/ec2/ec2-packages.git/commit/?id=d08c9d5ebd18a1d245238b0cd51d4b26eb849f74
s3_translate_name() {
    if [[ "$1" == "fix" ]]; then
        sed 's/+/ /g'
    elif [[ "$1" == "restore" ]]; then
        sed 's/ /+/g'
    else
        die "Unknown/missing argument \$1: ${1:-}"
    fi
}

# Prune extraneous packages from repo.
# CAUTION: This directly modifies the S3 bucket (regardless of --upload).
prune() {
    prune_removed
    prune_outdated
    # DB file needs to be kept in sync with package file changes.
    do_upload
}

prune_removed() {
    repo_pkgs=$(aur repo --list | awk '{print $1}' | sort)
    # Find with functions is *so* much fun...
    export -f srcinfo_get
    source_pkgs=$(find "$PROJECT_DIR/aur" "$PROJECT_DIR/packages" -name .SRCINFO \
                       -exec bash -c 'srcinfo_get pkgname "$@"' bash {} \; \
                      | sort)
    while IFS= read -r pkg; do
        # Emacs packages are in their own repo (emacs.d), so this prune script isn't aware of their
        # existence and would incorrectly remove them every run. So simple solution is to hardcode
        # an exclusion here. What could go wrong, right? (Fortunately not much, since worst case is
        # I end up with an extra couple cents/month on my S3 storage bill.)
        if [[ "$pkg" == chasecaleb-emacs* ]]; then
            continue
        fi

        repo-remove "$PROJECT_DIR/repo/$REPO_NAME.db.tar.xz" "$pkg"
        # aws-cli makes globs so straightforward, right?
        aws s3 rm "$REPO_BUCKET/" --recursive --exclude '*' --include "$pkg-*.pkg.*"
    done < <(comm -2 -3 <(echo "$repo_pkgs") <(echo "$source_pkgs"))
}

prune_outdated() {
    # Based on how aur-repo (aurutils) parses the db file.
    db_files=$(bsdcat "$PROJECT_DIR/repo/$REPO_NAME.db" \
                   | awk '/%FILENAME%/{getline; print $1}' \
                   | s3_translate_name fix \
                   | sort)

    s3_files=$(aws --output json s3api list-objects-v2 --no-paginate \
                   --bucket "$REPO_BUCKET_NAME" --prefix "$REPO_BUCKET_DIR/" \
                   | jq -e -r '.Contents[] | .Key' \
                   | sed "s@^$REPO_BUCKET_DIR/@@" \
                   | grep -vE "^$REPO_NAME\.(db|files).*" \
                   | sort)

    while IFS= read -r filename; do
        aws s3 rm "$REPO_BUCKET/$filename"
    done < <(comm -1 -3 <(echo "$db_files") <(echo "$s3_files"))
}

if [[ "$#" == 0 ]]; then
    die "Missing arguments"
fi

opt_init=0 opt_aur=0 opt_packages=0 opt_prune=0 opt_upload=0
while true; do
    case ${1:-} in
        --init)
            opt_init=1
            ;;
        --aur)
            opt_aur=1
            ;;
        --packages)
            opt_packages=1
            ;;
        --prune)
            opt_prune=1
            ;;
        --upload)
            opt_upload=1
            ;;
        --)
            # Preserve remaining args without parsing
            shift
            break
            ;;
        -?*)
            die "Unknown option: $1"
            ;;
        *)
            # No more options, done parsing
            break
            ;;
    esac
    shift
done


# Invoking in subshells to isolate state (global vars, cd, etc)
if ((opt_init)); then
    (init)
fi
if ((opt_aur)); then
    (build_aur)
fi
if ((opt_packages)); then
    (build_pkgs)
fi
if ((opt_prune)); then
    (prune)
fi
if ((opt_upload)); then
    (upload)
fi
