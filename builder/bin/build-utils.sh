#!/usr/bin/env bash
# Utility functions for package building.

set -Eeuo pipefail
die() { echo -e "$0" ERROR: "$@" >&2; exit 1; }
# shellcheck disable=2154
trap 's=$?; die "line $LINENO - $BASH_COMMAND"; exit $s' ERR


# Fetch repo DB files for my Arch Linux repo and do pacman -Syu.
# The pacman update is for two reasons:
# 1. At least -Sy (without -u) is necessary so that pacman reads/loads my custom repo db file.
# 2. Since we're about to build packages, it's a good idea to make sure the system packages are up
#    to date.
update_pacman_and_repo_dbs() {
    mkdir "$PROJECT_DIR/repo" 2>/dev/null || true
    local extension
    for extension in db files; do
        local name="$REPO_NAME.$extension.tar.xz"
        local dest="$PROJECT_DIR/repo/$name"
        aws s3 cp "$REPO_BUCKET/$name" "$dest"
        ln -sf "$dest" "$PROJECT_DIR/repo/$REPO_NAME.$extension"
    done

    sudo pacman -Syu --noconfirm
}

# Get value of field ($1) from .SRCINFO ($2).
srcinfo_get() {
    awk -vname="$1" '{ if ($1 == name) { print $3 } }' "$2"
}

# Updates pkgver (if dynamic) for PKGBUILD and .SRCINFO.
# Note: this also downloads sources.
update_pkgver() {
    # Subshell because of cd, etc.
    (
        cd "$1"
        makepkg --noprepare --nobuild --nodeps
        makepkg --printsrcinfo > .SRCINFO
    )
}

# Call update_pkgver for all PKGBUILDS found in $1 and its subdirectories (recursive).
update_all_pkgvers() {
    local dir=$1
    echo "Updating package versions in $dir"
    local file
    while IFS= read -rd '' file; do
        update_pkgver "$(dirname "$file")"
    done < <(find "$dir" -name PKGBUILD -print0 | sort -z)
}

# VCS packages have a pkgver() function to update their pkgver variable
# based on the source repo, so that needs to be run before checking if the
# package needs to be built.
update_pkgver_if_vcs_source() {
    local dir=$1
    local is_vcs=0
    local line
    while IFS= read -r line; do
        # VCS source are e.g. "foo::git+https://repo.git" (see man PKGBUILD)
        # Remove optional directory part ("foo::")
        local protocol=${line#*::}
        # Remove everything after protocol
        protocol=${protocol%%://*}
        if grep -qE "(bzr|git|hg|svn)" <<< "$protocol"; then
            is_vcs=1
            break
        fi
    done < <(srcinfo_get source "$dir/.SRCINFO")

    if ((is_vcs)); then
        echo "Updating pkgver for VCS package: $(basename "$dir")"
        update_pkgver "$dir"
    fi
}

# Check package versions against repo db and build any that are new/updated.
build_updated() {
    cd "$1"

    local all_pkgs
    all_pkgs=$(mktemp)
    # Get all package names in the correct build order (i.e. sorted by dependencies)
    find . -name .SRCINFO -print0 | xargs -0 aur graph -- | tsort | tac > "$all_pkgs"

    # Get local package versions
    local src_versions pkg
    src_versions=$(mktemp)
    while IFS= read -r pkg; do
        update_pkgver_if_vcs_source "$pkg"
        local src_info=$pkg/.SRCINFO

        # Even with split packages, there can only be a single version.
        # Source: https://wiki.archlinux.org/index.php/PKGBUILD
        # Implementation based on aurutil's aur-srcver.
        local pkg_ver pkg_rel full_ver epoch pkg_base
        pkg_ver=$(srcinfo_get pkgver "$src_info")
        pkg_rel=$(srcinfo_get pkgrel "$src_info")
        full_ver="$pkg_ver-$pkg_rel"
        epoch=$(srcinfo_get epoch "$src_info")
        if [[ "$epoch" != "" ]]; then
            full_ver="$epoch:$full_ver"
        fi

        # Using pkgbase because there can be multiple pkgname's in the case of
        # a split package, whereas pkgbase will always match the directory name.
        # This is also what aurutils (e.g. aur-sync) does, so it's not just me
        # doing something weird.
        pkg_base=$(srcinfo_get pkgbase "$src_info")
        echo -e "$pkg_base\t$full_ver" >> "$src_versions"
    done < "$all_pkgs"

    echo ".SRCINFO versions:"
    column -t "$src_versions" | sed 's/^/\t/'

    local build_queue
    build_queue=$(mktemp)
    # Existing packages that need to be updated
    aur repo --list | aur vercmp --quiet --path "$src_versions" > "$build_queue"
    # New packages to build for first time
    comm -2 -3 <(sort "$all_pkgs") <(aur repo --list | awk '{print $1}' | sort) >> "$build_queue"
    if [[ ! -s "$build_queue" ]]; then
        echo "All packages are up to date, nothing to build"
        return
    fi

    # $all_pkgs is sorted by build order, so use that to sort the build queue.
    local ordered_build_queue
    ordered_build_queue=$(mktemp)
    grep --file "$build_queue" "$all_pkgs" > "$ordered_build_queue"

    echo "Build queue:"
    sed 's/^/\t/' "$ordered_build_queue"
    aur build --arg-file="$ordered_build_queue" --margs "${@:2}"
}

# Always upload $PROJECT_DIR/repo packages (if any) and DB file.
do_upload() {
    # Make sure newly-built package files are in the db. Even though "aur build" also does this,
    # it's possible to build and upload packages in separate CI jobs. This allows me to build my
    # packages and AUR packages in parallel, then upload them sequentially while keeping the shared
    # DB file in sync.
    for pkg in "$PROJECT_DIR/repo/"*.pkg.*; do
        # Test for existence in case glob didn't match anything. This is particularly the case when
        # pruning removed packages.
        if [[ -e "$pkg" ]]; then
            # --prevent-downgrade for the sake of paranoia. Even if CI jobs run out of order this
            # still shouldn't be an issue, since my build script only builds new versions of
            # packages, but I still want this as an extra safeguard. Obviously this will prevent me
            # from intentionally pushing an older version of a package, but that's ok because I can
            # roll forward or manually intervene if that ever becomes necessary.
            repo-add --prevent-downgrade "$PROJECT_DIR/repo/$REPO_NAME.db.tar.xz" "$pkg"
        fi
    done

    local filename
    for filename in "$PROJECT_DIR/repo/"*+*; do
        if [[ -e "$filename" ]]; then
            mv "$filename" "$(s3_translate_name fix <<< "$filename")"
        fi
    done

    aws s3 sync "$PROJECT_DIR/repo/" "$REPO_BUCKET/" --exclude '*.old'
}

# Maybe upload, if any packages were built.
upload() {
    # Don't waste bandwidth uploading unchanged db files. Also this makes it easier to understand
    # what the script is doing when watching its output.
    if (( $(find "$PROJECT_DIR/repo" -type f -name '*.pkg.*' | wc -l) )); then
        # Make sure an up-to-date repo db file is used.
        update_pacman_and_repo_dbs
        do_upload
    else
        echo "No packages to upload"
    fi
}
