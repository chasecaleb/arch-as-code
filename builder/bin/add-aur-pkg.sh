#!/usr/bin/env bash
# Add AUR package source (PKGBUILD/etc) to this project.

set -Eeuo pipefail
die() { echo -e "$0" ERROR: "$@" >&2; exit 1; }
# shellcheck disable=2154
trap 's=$?; die "line $LINENO - $BASH_COMMAND"; exit $s' ERR

if [[ "$#" != 2 ]]; then
    die "Expected exactly two arguments: project directory and AUR package name"
fi
AUR_DIR=$1/aur
PKG_NAME=$2

echo "Fetching package $PKG_NAME"
cd "$AUR_DIR"
rm -rf "./$PKG_NAME" 2>/dev/null || true
aur fetch "$PKG_NAME"
# Yes, I *could* use submodules... but I want to have my own mirrored copy instead.
rm -rf "$PKG_NAME/.git"
# Some packages have rather eccentric .gitignore's that cause problems. For
# example: kubernetes-helm (as of 03/18/2020, version 3.1.2-1) has a single
# line: "*".
# Fortunately I can blindly throw out all of their gitignore's since the only
# time I build is within a throwaway container.
rm "$PKG_NAME/.gitignore" 2>/dev/null || true
