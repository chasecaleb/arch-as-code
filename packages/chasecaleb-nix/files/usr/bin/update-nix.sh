#!/usr/bin/env bash
# Update Nix Home Manager config for current user.

set -Eeuo pipefail
die() { echo -e "$0" ERROR: "$@" >&2; exit 1; }
# shellcheck disable=2154
trap 's=$?; die "line $LINENO - $BASH_COMMAND"; exit $s' ERR

if [[ "${SUDO_UID:-}" ]]; then
    systemd-run --wait --pipe --quiet --uid="$SUDO_UID" "$(realpath "${BASH_SOURCE[0]}")" 2>&1 | sed 's/^/\t/'
elif [[ "$UID" == 0 ]]; then
    echo "Running as root user without sudo, skipping user-specific Nix Home Manager update" >&2
else
    echo "Updating home-manager config"
    # Needs to be absolute path due to limited env of machinectl.
    config_dir=/home/caleb/code/arch-as-code/nix-home-config
    ~/.nix-profile/bin/home-manager switch --flake "path:$config_dir#caleb"
fi
