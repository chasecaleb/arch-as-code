#!/usr/bin/env bash
# User configuration for Nix.
#
# Home manager install doc:
#   https://nix-community.github.io/home-manager/index.html#sec-install-standalone

# Reminder: no -Eeuo pipefail because this runs from zshrc.

# Nix Home Manager config is in my arch as code repo.
# Also, using /home/caleb/ explicitly instead of ~ in case I have a moment of insanity and suddenly
# create multiple user accounts, or... something.
config_dir_source=/home/caleb/code/arch-as-code/nix-home-config
config_dir_link=~/.config/nixpkgs

do_configure() {
    if ! nix-channel --list | awk '{print $1}' | grep '^nixpkgs$' >/dev/null; then
        nix-channel --add https://nixos.org/channels/nixpkgs-unstable
        nix-channel --update
    fi

    if ! nix-channel --list | awk '{print $1}' | grep 'home-manager' >/dev/null; then
        nix-channel --add https://github.com/nix-community/home-manager/archive/master.tar.gz home-manager
        nix-channel --update
    fi

    export NIX_PATH=$HOME/.nix-defexpr/channels:/nix/var/nix/profiles/per-user/root/channels${NIX_PATH:+:$NIX_PATH}
    if ! which home-manager >/dev/null 2>&1; then
        echo "Installing home manager"
        nix build --no-link "path:$config_dir_source#homeConfigurations.$(whoami).activationPackage"
        "$(nix path-info "$config_dir_source#homeConfigurations.$(whoami).activationPackage")/bin/activate"
    fi

    # shellcheck disable=SC1090
    source ~/.nix-profile/etc/profile.d/hm-session-vars.sh

    if [[ -e "$config_dir_link" && ! -L "$config_dir_link" ]]; then
        rm -rf "$config_dir_link"
    fi
    if [[ ! -e "$config_dir_link" ]]; then
        ln -s "$config_dir_source" "$config_dir_link"
    fi

    # Alias for interactive use, because this gets old to type out by hand.
    hm() {
        home-manager --flake "path:$config_dir_source#$(whoami)" "$@"
    }
}

# Don't want to use home manager for root.
if [[ "$UID" != 0 ]]; then
    do_configure
fi
