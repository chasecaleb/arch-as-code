#!/usr/bin/env bash
# Change display resolution. Useful for screen sharing from a 4k screen.

set -Eeuo pipefail
die() { echo -e "$0" ERROR: "$@" >&2; exit 1; }
# shellcheck disable=2154
trap 's=$?; die "line $LINENO - $BASH_COMMAND"; exit $s' ERR

# Create/add new xrandr mode if not already available for current output, because VM guest
# tools/graphic drivers don't include them (whether on VMware Fusion, VMware Workstation, or
# Parallels Desktop)... which more or less makes sense, since VMs don't have physical monitors.
add_mode() {
    local mode_name=$1
    if ! xrandr | grep --quiet "$mode_name" >/dev/null; then
        xrandr --newmode "$@"
        local monitor
        monitor=$(xrandr --listactivemonitors | awk '/^ 0:/ {print $NF}')
        xrandr --addmode "$monitor" "$mode_name"
    fi
}

if [[ $# == 0 || "$1" == "default" ]]; then
    # "Preferred modes" (in xrandr nomenclature) are indicated by a "+" (plus sign).
    # xrandr --preferred does this in theory, but Parallels Desktop also adds a hardcoded 1024x768
    # resolution that's marked as preferred.  Instead of messing around with xorg.conf files it's
    # easier to just pick the non-1024 preferred resolution here.
    xrandr --size "$(xrandr | awk '/^\s+.*\+/ {print $1}' | sort -rn | head -1)"
elif [[ "$1" == "1080" ]]; then
    # More mode lines can be generated via "cvt" (part of xorg-server package)
    add_mode "1920x1080_60.00"  173.00  1920 2048 2248 2576  1080 1083 1088 1120 -hsync +vsync
    xrandr --size 1920x1080
elif [[ "$1" == "720" ]]; then
    add_mode "1280x720_60.00"   74.50  1280 1344 1472 1664  720 723 728 748 -hsync +vsync
    xrandr --size 1280x720
else
    die "Unknown argument: $1"
fi

echo "Success. Resize the VM window on the host to go back to auto resolution."
