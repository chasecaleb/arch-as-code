#!/usr/bin/env bash
# Config management hook for holo, systemctl, etc.
# This must be run as a superuser.

set -Eeuo pipefail
die() { echo -e "$0" ERROR: "$@" >&2; exit 1; }
# shellcheck disable=2154
trap 's=$?; die "line $LINENO - $BASH_COMMAND"; exit $s' ERR

# Warn about packages that are not supposed to be installed. Removing them
# instead of warning would be nice, but pacman doesn't allow it when running
# in a PostTransaction hook.
check_packages() {
    local ORPHANS EXPLICITS
    # -t a second time includes packages that are only optional dependencies.
    # pacman exits 1 if there are no packages.
    ORPHANS=$(pacman -Qqdtt || [[ $? == 1 ]])
    if [[ "$ORPHANS" ]]; then
        echo "WARNING: Found orphaned packages:"
        # shellcheck disable=2001
        sed 's/^/\t/' <<< "$ORPHANS"
    fi

    # The only packages that should be explicitly installed are my own.
    EXPLICITS=$(pacman -Qqe | grep -v chasecaleb || [[ $? == 1 ]])
    if [[ "$EXPLICITS" ]]; then
        echo "WARNING: Found unexpected explicitly installed packages:"
        # shellcheck disable=2001
        sed 's/^/\t/' <<< "$EXPLICITS"
    fi

    if [[ "$EXPLICITS" || "$ORPHANS" ]]; then
        local RM_EXPLICITS=""
        if [[ "$EXPLICITS" ]]; then
            # Yes shellcheck, I know single quotes don't expand. That's the point.
            # shellcheck disable=2016
            RM_EXPLICITS='sudo pacman -D --asdeps $(pacman -Qqe | grep -v chasecaleb) && '
        fi
        # shellcheck disable=2016
        local RM_ORPHANS='sudo pacman -Rns $(pacman -Qqdtt)'
        printf 'To remedy, run: %s%s\n' "$RM_EXPLICITS" "$RM_ORPHANS"
    fi
}

check_lost_files() {
    local LOST_FILES
    # systemd units are ignored because I manage them via systemctl preset-all
    # (also because there I don't want to maintain a list of them here...).
    LOST_FILES=$(lostfiles \
                     | grep -v --line-regexp \
                            -e '/boot/.*' \
                            -e '/etc/brlapi.key' \
                            -e '/etc/docker' \
                            -e '/etc/docker/key.json' \
                            -e '/etc/fonts/conf.d/.*\.conf' \
                            -e '/etc/mkinitcpio.d/linux-zen.preset' \
                            -e '/etc/systemd/user/.*\.target\..*' \
                            -e '/etc/unbound/trusted-key\.key' \
                            -e '/etc/unbound/dev.*' \
                            -e '/etc/unbound/run.*' \
                            -e '/etc/wpa_supplicant/wpa_supplicant-.*\.conf' \
                            -e '/etc/xml' \
                            -e '/opt/containerd' \
                            -e '/opt/containerd/.*' \
                            -e '/usr/.*\.cache' \
                            -e '/usr/lib/graphviz/config.*' \
                            -e '/usr/lib/modules/.*/kernel/.*' \
                            -e '/usr/lib/udev/hwdb\.bin' \
                            -e '/usr/share/fonts/.*\.dir' \
                            -e '/usr/share/fonts/.*\.scale' \
                            -e '/usr/share/glib-2\.0/schemas/.*\.compiled' \
                            -e '/usr/share/vim/vimfiles/doc/tags' \
                     || [[ $? == 1 ]])
    if pacman -Qq chasecaleb-parallels-guest-tools >/dev/null 2>&1; then
        # Parallels guest tools has an 1,800 line install script, so trying to recreate that myself
        # in order to package them properly is one rabbit hole that I'm not willing to go down.
        # Maintaining this ignore list is quite a few orders of magnitude simpler and good enough.
        LOST_FILES=$(grep -v --line-regexp \
                          -e '/etc/modprobe\.d/blacklist-parallels\.conf' \
                          -e '/etc/modprobe\.d/prl_eth\.conf' \
                          -e '/etc/udev/rules\.d/99-parallels-video\.rules' \
                          -e '/etc/X11/xorg\.conf\.d/90-prlmouse\.conf' \
                          -e '/etc/xdg/autostart/prlcc\.desktop' \
                          -e '/etc/xdg/autostart/ptiagent\.desktop' \
                          -e '/usr/bin/prl.*' \
                          -e '/usr/bin/ptiagent' \
                          -e '/usr/bin/ptiagent-cmd' \
                          -e '/usr/lib/libEGL\.so[0-9.]*' \
                          -e '/usr/lib/libgbm\.so[0-9.]*' \
                          -e '/usr/lib/libGL\.so[0-9.]*' \
                          -e '/usr/lib/libPrlDRI\.so[0-9.]*' \
                          -e '/usr/lib/libPrlWl\.so[0-9.]*' \
                          -e '/usr/lib/modules/[^/]*' \
                          -e '/usr/lib/modules/[^/]*/extra' \
                          -e '/usr/lib/modules/[^/]*/extra/prl_.*\.ko' \
                          -e '/usr/lib/parallels-tools' \
                          -e '/usr/lib/parallels-tools/.*' \
                          -e '/usr/lib/udev/rules\.d/69-xorg-prlmouse\.rules' \
                          -e '/usr/lib/xorg/modules/drivers/prlvide[lox]_drv\.so' \
                          -e '/usr/lib/xorg/modules/extensions/libglx\.so[0-9.]*' \
                          -e '/usr/lib/xorg/modules/input/prlmouse_drv\.so' \
                          -e '/usr/share/icons/.*/parallels-tools\.png' \
                          -e '/usr/share/man/man8/mount\.prl_fs\.8' \
                          -e '/usr/share/X11/xorg\.conf\.d/40-prltools\.conf' \
                          -e '/usr/share/X11/xorg\.conf\.d/90-prlmouse\.conf' \
                          -e '/usr/src/parallels-tools-[0-9.]*' \
                          -e '/usr/src/parallels-tools-[0-9.]*/.*' \
                          <<< "$LOST_FILES" \
                  || [[ $? == 1 ]])
    fi

    if [[ "$LOST_FILES" ]]; then
        echo "WARNING: Found lost files:"
        # shellcheck disable=2001
        sed 's/^/\t/' <<< "$LOST_FILES"
    fi
}

if [[ "${SUDO_UID:-}" ]]; then
    machinectl shell --quiet --uid="$SUDO_UID" .host /usr/bin/chasecaleb-git-repos
fi

holo apply
locale-gen
sysctl --quiet --system >/dev/null
systemctl daemon-reload
systemctl preset-all
if [[ "${SUDO_UID:-}" ]]; then
    # Today I found out that machinectl exists as part of systemd. This is a
    # much nicer way of reaching back into a user's session than e.g. using
    # sudo and making assumptions about $XDG_RUNTIME_DIR and
    # $DBUS_SESSION_BUS_ADDRESS.
    machinectl shell --quiet --uid="$SUDO_UID" .host /usr/bin/systemctl --user daemon-reload
fi
systemctl --global preset-all

check_packages >&2
check_lost_files >&2
