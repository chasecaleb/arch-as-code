# Docs: https://github.com/polybar/polybar

[settings]
screenchange-reload = true

[global/wm]
margin-top = 0
margin-bottom = 0

# Colors based on spacemacs-dark
[colors]
background = #222226
foreground = #b2b2b2
background-alt = #5d4d7a
foreground-alt = #b2b2b2
background-higlight = #eead0e
foreground-higlight = #3e3d31

[bar/panel]
offset-x = 0
offset-y = 0
fixed-center = true
enable-ipc = true

background = ${colors.background}
foreground = ${colors.foreground}

border-bottom-size = 3
border-color = ${colors.background-alt}

padding-left = 1
padding-right = 1

module-margin = 1
separator = "|"

font-0 = "Hack:size=10;2"
# See https://fonts.google.com/icons to find icons, then insert with "C-x 8 RET <codepoint>"
font-1 = "Material Icons:size=12;4"

modules-left = emacs-workspace pkg-updates dunst-status emacs-slack emacs-irc
modules-center = emacs-org
modules-right = audio battery hostname date

tray-position = right
tray-padding = 2
tray-offset-y = -1

[module/emacs-workspace]
type = custom/ipc
hook-0 = emacsclient -e '(cc/panel-data-workspace)' | sed -e 's/^"//' -e 's/"$//'
initial = 1

# Note: colors for emacs-org defined via inline format tags (see hook implementation in Emacs).
[module/emacs-org]
type = custom/ipc
hook-0 = emacsclient -e '(cc/panel-data-org)' | sed -e 's/^"//' -e 's/"$//'
initial = 1

[module/emacs-slack]
type = custom/ipc
hook-0 = emacsclient -e '(cc/panel-data-slack)' | sed -e 's/^"//' -e 's/"$//'
initial = 1
format-background = ${colors.background-higlight}
format-foreground = ${colors.foreground-higlight}
format-padding = 1

[module/emacs-irc]
type = custom/ipc
hook-0 = emacsclient -e '(cc/panel-data-irc)' | sed -e 's/^"//' -e 's/"$//'
initial = 1
format-background = ${colors.background-higlight}
format-foreground = ${colors.foreground-higlight}
format-padding = 1

[module/date]
type = internal/date
interval = 5
date = "%A %B %d"
time = "%I:%M %p"
label = %date% %time%

[module/battery]
type = internal/battery
full-at = 95 # /shrug this seems to be the max with Parallels Desktop on Mac.
# https://github.com/polybar/polybar/wiki/Module:-battery
# ls -l /sys/class/power_supply
battery = BAT0
adapter = ADP0
# Fallback in case inotify isn't supported by battery
poll-interval = 15
label-charging = " %percentage%% / %time%"
label-discharging = " %percentage%% / %time%"
label-full = " Full"

[module/audio]
type = internal/pulseaudio
use-ui-max = false
label-muted =  Mute
format-muted = <label-muted>
format-volume =  <label-volume>

[module/pkg-updates]
type = custom/script
exec = chasecaleb-checkupdates-status
format-background = ${colors.background-alt}
format-foreground = ${colors.foreground-alt}
format-padding = 1
interval = 600

[module/dunst-status]
type = custom/script
exec = "[[ $(dunstctl is-paused) == 'true' ]] && echo 'Notifications '"
interval = 5
format-background = ${colors.background-alt}
format-foreground = ${colors.foreground-alt}
format-padding = 1

[module/hostname]
# I use KVM switching between work laptop and personal desktop, so... yeah.
type = custom/script
exec = uname -n
# Hostname won't ever change, so set an arbitrarily high interval.
interval = 99999
