#!/usr/bin/env bash
# Copy GPG configuration, since GPG does not have system-wide config files.
# This does NOT import keys.

set -Eeuo pipefail
die() { echo -e "$0" ERROR: "$@" >&2; exit 1; }
# shellcheck disable=2154
trap 's=$?; die "line $LINENO - $BASH_COMMAND"; exit $s' ERR

gpg_dir=$HOME/.gnupg
if [[ ! -e "$gpg_dir" ]]; then
    mkdir "$gpg_dir"
fi
cp -r /etc/skel/.gnupg/* "$gpg_dir/"
find "$gpg_dir" -type d -exec chmod 700 {} \+
find "$gpg_dir" -type f -exec chmod 600 {} \+
