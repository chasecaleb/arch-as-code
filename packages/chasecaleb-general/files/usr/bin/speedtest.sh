case $1 in
    10|s|sm|small)
        echo "Using small (10mb) file"
	curl -o /dev/null http://speedtest.wdc01.softlayer.com/downloads/test10.zip
	;;
    500|l|lg|lrg|large)
        echo "Using large (500mb) file"
	curl -o /dev/null http://speedtest.wdc01.softlayer.com/downloads/test500.zip
	;;
    100|m|med|medium|*)
        echo "Using medium (100mb) file"
	curl -o /dev/null http://speedtest.wdc01.softlayer.com/downloads/test100.zip
	;;
esac
