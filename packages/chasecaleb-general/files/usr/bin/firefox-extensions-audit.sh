#!/bin/bash
# Firefox has the horrible security/UX flaw of abbreviating domain permissions with no way of showing them all... so I made an audit script.
FF_DIR=$HOME/.mozilla/firefox
FF_PROFILE=$(awk -F "=" '/Default/ {print $2}' "$FF_DIR/installs.ini")

# Only interested in 3rd-party addons, not ones that ship with Firefox.
jq -C \
   '.addons[] | select(.location == "app-profile") | {name: .defaultLocale.name, userPermissions}' \
   "$FF_DIR/$FF_PROFILE/extensions.json" | less -p '"name"'
