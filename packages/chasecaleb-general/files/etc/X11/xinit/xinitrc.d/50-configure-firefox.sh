#!/usr/bin/env bash
# Firefox doesn't sync userChrome.css and doesn't have a system-wide equivalent.
FF_DIR=$HOME/.mozilla/firefox
FF_PROFILE=$(awk -F "=" '/Default/ {print $2}' "$FF_DIR/installs.ini" || true)
# This has to be an if-else instead of exit 0 because it's sourced by the login shell, so exiting
# would abort the login.
if [[ ! -e "$FF_PROFILE" ]]; then
    echo "Skipping Firefox configuration (must be run at least once first)"
else
    CHROME_DIR=$FF_DIR/$FF_PROFILE/chrome/

    if [[ ! -e "$CHROME_DIR" ]]; then
        mkdir "$CHROME_DIR"
    fi
    cat > "$CHROME_DIR/userChrome.css" << EOF
#titlebar { display: none }
EOF

    # There are system-wide equivalents to this user.js, but since userChrome.css is modified
    # per-profile I'm sticking with that for consistency. Plus this keeps all my firefox modification in
    # one place.

    cat > "$FF_DIR/$FF_PROFILE/user.js" << EOF
user_pref("browser.aboutConfig.showWarning", false);
// backspace_action: 0 is history back (and forward with shift+backspace)
user_pref("browser.backspace_action", 0);
user_pref("browser.bookmarks.showMobileBookmarks", true);
user_pref("browser.ctrlTab.recentlyUsedOrder", false);
user_pref("browser.newtabpage.enabled", false);
user_pref("browser.protections_panel.infoMessage.seen", true);
user_pref("browser.rights.3.shown", true);
user_pref("browser.startup.homepage", "about:blank");
user_pref("browser.tabs.warnOnClose", false);

user_pref("devtools.chrome.enabled", true);

user_pref("dom.forms.autocomplete.formautofill", true);

user_pref("findbar.highlightAll", true);

// I used to have this set to true because I also had Firefox synced to my Windows desktop and
// didn't want all of the same extensions enabled. That isn't the case anymore, so I'm explicitly
// reverting this back to the default of false to make sure it gets applied everywhere.
user_pref("services.sync.addons.ignoreUserEnabledChanges", false);

// Use tab-less windows (along with I Hate Tabs extension)
user_pref("browser.tabs.opentabfor.middleclick", false);
user_pref("toolkit.legacyUserProfileCustomizations.stylesheets", true);

// SSB = site specific browser, aka "app mode"
user_pref("browser.ssb.enabled", true);

// Disable TRR (aka built-in DNS-over-HTTPS) and use system DNS instead.
// I use systemd-resolved with DoH, so I don't want an app-specific implementation.
// https://support.mozilla.org/en-US/kb/firefox-dns-over-https#w_manually-enabling-and-disabling-dns-over-https
user_pref("network.trr.mode", 5);

// org-roam generates random files in /tmp, so Firefox's "Allow this file to open..." prompt shows
// up each time the file is recreated even if the "always allow this file" checkbox is enabled.
user_pref("network.protocol-handler.external.org-protocol", true);
EOF

fi
