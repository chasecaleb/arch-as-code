#!/usr/bin/env bash
# Note: pacman/makepkg show a warning (instead of aborting) if anything in this install script has
# a non-zero return or exit code, which is unfortunate... so pay attention to the output.

set -Eeuo pipefail
die() { echo -e "$0" ERROR: "$@" >&2; exit 1; }
# shellcheck disable=2154
trap 's=$?; die "line $LINENO - $BASH_COMMAND"; exit $s' ERR

MNT_DIR=/mnt/parallels-guest-tools

post_install() {
    if [[ ! -d "$MNT_DIR" ]]; then
        mkdir "$MNT_DIR"
    fi
    if [[ ! -f "$MNT_DIR/version" ]]; then
        mount /dev/sr0 "$MNT_DIR"
    fi
    echo "Found Parallels guest tools version $(cat $MNT_DIR/version)"

    echo "Patching..."
    local PATCH_DIR KMODS_DIR
    PATCH_DIR=$(mktemp -d)
    KMODS_DIR=$(mktemp -d)
    echo "patch dir $PATCH_DIR"
    echo "kmods dir $KMODS_DIR"
    cp -r "$MNT_DIR"/* "$PATCH_DIR"
    cd "$KMODS_DIR"
    tar xpf "$PATCH_DIR/kmods/prl_mod.tar.gz"
    patch -p1 < /usr/share/chasecaleb-parallels-guest-tools/kernel.patch
    tar czpf "$PATCH_DIR/kmods/prl_mod.tar.gz" .

    echo "Running Parallels guest tools installer..."
    "$PATCH_DIR/install" --install-unattended-with-deps --verbose --restore-on-fail

    rm -rf "$PATCH_DIR"
    echo "Guest tools installed successfully"
}

post_upgrade() {
    post_install
}
