#!/usr/bin/env bash
# Config for kubernetes' kubectl and kubectx.
set -Eeuo pipefail
die() { echo -e "$0" ERROR: "$@" >&2; exit 1; }
# shellcheck disable=2154
trap 's=$?; die "line $LINENO - $BASH_COMMAND"; exit $s' ERR

# I'm using a system file for config such as my work EKS clusters, but I also want to be able to
# change temporary things such as current-context. In order for kubectl to write to the user file,
# it needs to actually exist (otherwise kubectl will skip it and modify the next file in
# $KUBECONFIG, which I don't want it to do).
USER_CONFIG_DIR=$HOME/.kube
USER_CONFIG=$USER_CONFIG_DIR/config
if [[ ! -d "$USER_CONFIG_DIR" ]]; then
    mkdir "$USER_CONFIG_DIR"
fi
if [[ ! -f "$USER_CONFIG" ]]; then
    touch "$USER_CONFIG"
fi

# Workaround for kubectl's stupidity about config file writing.
# See https://github.com/kubernetes/kubernetes/issues/67676
SYSTEM_CONFIG_LINK=$USER_CONFIG_DIR/system
ln -sf /mnt/chasecaleb-work-mac-secrets/kube-config "$SYSTEM_CONFIG_LINK"

export KUBECONFIG="$USER_CONFIG:$SYSTEM_CONFIG_LINK"
