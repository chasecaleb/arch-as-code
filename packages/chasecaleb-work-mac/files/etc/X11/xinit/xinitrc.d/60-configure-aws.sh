#!/usr/bin/env bash
# aws-cli config file location.
#
# Notice that this file has a higher number than the aws config in chasecaleb-dev, so this one will
# win since it runs last.

set -Eeuo pipefail
die() { echo -e "$0" ERROR: "$@" >&2; exit 1; }
# shellcheck disable=2154
trap 's=$?; die "line $LINENO - $BASH_COMMAND"; exit $s' ERR

export AWS_CONFIG_FILE=/mnt/chasecaleb-work-mac-secrets/aws-config
