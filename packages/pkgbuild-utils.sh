#!/usr/bin/env bash
# Shared PKGBUILD functionality.

# Make shellcheck to shut up, since these are provided by makepkg/PKGBUILD
declare pkgname pkgdir

# Compute pkgver based on revision count (so DON'T REBASE/SQUASH in master).
_get_pkgver() {
    REV_COUNT=$(git rev-list --count HEAD -- "$@")
    REV_HASH=$(git rev-list --max-count=1 HEAD --abbrev-commit -- "$@")
    # Date isn't strictly necessary, but it's helpful for my sanity.
    REV_DATE=$(TZ=UTC git log -1 --pretty='format:%ad' --date='format-local:%Y%m%d.%H%M%S' -- "$@")
    printf "r%s.%s.%s" "$REV_COUNT" "$REV_DATE" "$REV_HASH"
}

_do_install() {
    local FILE=$1
    local DEST=$2

    # install can't handle symlinks.
    if [[ -L "$FILE" ]]; then
        local DEST_DIR
        DEST_DIR=$(dirname "$DEST")
        if [[ ! -d "$DEST_DIR" ]]; then
            mkdir -p "$DEST_DIR"
        fi
        cp --no-dereference --preserve=link "$FILE" "$DEST"
    else
        install -Dm644 "$FILE" "$DEST"
    fi

    if [[ -x "$FILE" ]]; then
        chmod +x "$DEST"
    fi
}

_do_package_secrets() {
    local PKG_DIR=$1
    local MNT=/mnt/$pkgname-secrets
    install --directory "$pkgdir/$MNT"

    _do_install "$PKG_DIR/encrypted-key.gpg" "$pkgdir/etc/$pkgname-encrypted-key.gpg"

    local CIPHER_DIR=/etc/$pkgname-encrypted
    local FSTAB=$pkgdir/usr/share/holo/files/05-$pkgname-secrets/etc/fstab.holoscript
    install --directory "$(dirname "$FSTAB")"
    # noauto,x-systemd.amount defers mounting until first access, which is important since
    # emacsclient needs to be running because it's used for gpg passphrase entry. Additionally, I
    # don't want to enter my gpg passphrase until/unless I actually need to, because I'm lazy.
    cat <<EOF > "$FSTAB"
#!/usr/bin/env bash
# Auto-generated secrets mount for package $pkgname

set -Eeuo pipefail
die() { echo -e "\$0" ERROR: "\$@" >&2; exit 1; }
# shellcheck disable=2154
trap 's=\$?; die "line \$LINENO - \$BASH_COMMAND"; exit \$s' ERR

# Preserve existing fstab contents and append secrets mount.
cat -
echo "$CIPHER_DIR $MNT fuse.gocryptfs noauto,x-systemd.automount,nofail,allow_other,extpass=chasecaleb-gocryptfs-password,extpass=$pkgname 0 0"
EOF
    chmod +x "$FSTAB"

    # Subshell to undo directory change.
    (
        # shellcheck disable=2164
        cd "$PKG_DIR/encrypted"
        while IFS= read -rd '' FILE; do
            # shellcheck disable=2154
            _do_install "$FILE" "$pkgdir/$CIPHER_DIR/$FILE"
        done < <(find . \( -type f -o -type l \) -print0)
    )
}
