#!/usr/bin/env bash
# nvm is user specific (since the whole point is to manage node versions on a transient basis), so
# it's here in xinitirc.d to accomplish that.

set -Eeuo pipefail
die() { echo -e "$0" ERROR: "$@" >&2; exit 1; }
# shellcheck disable=2154
trap 's=$?; die "line $LINENO - $BASH_COMMAND"; exit $s' ERR

# nvm package includes a very similar script at /usr/share/nvm/init-nvm.sh,
# but it doesn't quite work the way I want so I'm not sourcing it. For instance:
# - Breaks with -Eeuo pipefail
# - Has a default/fallback for $NVM_DIR, but doesn't create the dir if needed.
# Also, it's a four liner so I don't feel bad about maintaining my own version here.

if [[ "${NVM_DIR:-}" == "" ]]; then
    export NVM_DIR="$HOME/.nvm"
fi
if [[ ! -d "$NVM_DIR" ]]; then
    mkdir "$NVM_DIR"
fi

source /usr/share/nvm/nvm.sh
# Note: this bash_completion script works for zsh too.
source /usr/share/nvm/bash_completion
source /usr/share/nvm/install-nvm-exec
