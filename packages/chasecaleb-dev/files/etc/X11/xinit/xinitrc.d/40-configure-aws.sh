#!/usr/bin/env bash
# aws-cli config file location.
#
# Notice that this file has a lower number than the aws config in chasecaleb-work-mac, so that one
# will run last and therefore take precedence if installed.

set -Eeuo pipefail
die() { echo -e "$0" ERROR: "$@" >&2; exit 1; }
# shellcheck disable=2154
trap 's=$?; die "line $LINENO - $BASH_COMMAND"; exit $s' ERR

export AWS_CONFIG_FILE=/mnt/chasecaleb-dev-secrets/aws-config
