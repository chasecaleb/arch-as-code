#!/usr/bin/env bash
# Glab's configurability has a few issues:
#
# 1. Only reads auth token from an env variable or from its unencrypted config file. I don't want
#    sensitive data stored unencrypted at rest, nor do I want it in an env variable shared by
#    everything. I also don't want to set it in my global env because that would require unlocking
#    my GPG key immediately on login.
# 2. No system config file location, only user specific. To be fair this makes sense for a tool like
#    this, but my arch-as-code config management works better with system files.

get_token() {
    # Need "|| true" because read exits non-zero upon encountering EOF marker.
    read -r -d '' lisp_cmd <<EOF || true
(auth-source-pick-first-password :host "glab" :user "$1")
EOF
    emacsclient --eval "$lisp_cmd" | sed -e 's/^"//' -e 's/"$//'
}

if git remote -v | grep --quiet 'gitlab.com.*NAIC'; then
    GITLAB_TOKEN=$(get_token cchase)
else
    GITLAB_TOKEN=$(get_token chasecaleb)
fi
# Remember that exported variables don't propogate up to parent process, so this is temporary.
export GITLAB_TOKEN
# Glab reads config file from $XDG_CONFIG_HOME/glab-cli (if set).
export XDG_CONFIG_HOME=/etc

exec /usr/bin/glab "$@"
